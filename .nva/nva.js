const pxToViewport = require('postcss-px-to-viewport')
const viewportUnits = require('postcss-viewport-units')
const OfflinePlugin = require('offline-plugin')
const glob = require('glob')
const { resolve } = require('path')

module.exports = {
  type: 'frontend',
  spa: true,
  autocheck: ['react', 'react-dom'],
  jsExt: '.jsx',
  cssExt: '.styl',
  watch: ['.stylelintrc'],
  outputPrefix: '/',
  postcss: {
    sourceMap: 'inline',
    plugins: [
      pxToViewport({
        viewportWidth: 375,
        unitPrecision: 3,
        viewportUnit: 'vw',
        selectorBlackList: ['.ignore', '.hairlines'],
        minPixelValue: 1,
        mediaQuery: false
      }),
      viewportUnits()
    ]
  },
  beforeBuild(conf) {
    const vendors = glob.sync('dist/vendor/*.@(js|css)').map(v => v.replace('dist', ''))
    const assets = glob.sync('dist/asset/**/*').map(v => v.replace('dist', ''))

    let offlinePlugin = new OfflinePlugin({
      appShell: '/',
      caches: {
        main: [':rest:'],
        additional: [':externals:']
      },
      cacheMaps: [
        {
          match: function(requestUrl) {
            return new URL('/', location);
          },
          requestTypes: ['navigate']
        }
      ],
      ServiceWorker: {
        publicPath: '/',
        events: true,
        entry: resolve('.nva', 'service-worker.js'),
        output: 'sw.js'
      },
      safeToUseOptionalCaches: true,
      externals:['/'].concat(vendors, assets)
    })
    return { plugins: conf.plugins.concat([offlinePlugin]) }
  }
}
