const cacheName = 'mocks'
self.addEventListener('fetch', (event) => {
  const requestUrl = new URL(event.request.url)
  if (requestUrl.pathname.startsWith('/mock/')) {
    event.respondWith(
      caches.open(cacheName).then(cache => {
        return fetch(event.request).then(networkResponse => {
          cache.put(event.request, networkResponse.clone())
          return networkResponse
        }).catch(()=>cache.match(event.request))
      })
    )
  }
  // event.respondWith(
  //   caches.match(event.request).then(function(response) {
  //     return response || fetch(event.request)
  //   })
  // )
})
