import axios from 'axios'
// import offline from '../util/offline'

// const offlineAdapter = offline({
//   name: 'axios-offline',
//   adapter: axios.defaults.adapter
// })

// const http = axios.create({
//   adapter: offlineAdapter
// })

export function events () {
  return axios.get('/mock/events').then(ret => ret.data.data)
}
