import axios from 'axios'
import offline from '../util/offline'

const offlineAdapter = offline({
  name: 'axios-offline',
  adapter: axios.defaults.adapter
})

const http = axios.create({
  adapter: offlineAdapter
})

export function event ({ id }) {
  return http.get(`/mock/event/${id}`).then(ret => ret.data.data)
}
