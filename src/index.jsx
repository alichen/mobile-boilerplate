import React from 'react'
import { render } from 'react-dom'
import Router from './router.jsx'
import lozad from 'lozad'

const observer = lozad()
observer.observe()

if (process.env.NODE_ENV === 'production') {
  const OfflinePluginRuntime = require('offline-plugin/runtime')
  OfflinePluginRuntime.install({
    onUpdateReady: () => OfflinePluginRuntime.applyUpdate(),
    onUpdated: () => {
      console.log('updated')
    }
  })
}

const hacks = require('viewport-units-buggyfill/viewport-units-buggyfill.hacks')
require('viewport-units-buggyfill').init({
  hacks: hacks
})

render(<Router />, document.getElementById('app'))
